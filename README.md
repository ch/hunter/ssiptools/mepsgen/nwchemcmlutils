# NWChemCMLUtils #

Module to create and run NWChem calculations to produce MEPS and CML descriptions for Surface Site Interaction Point
(SSIP) description generation.
A simple CLI is included to set up jobs on SLURM based HPC systems, with default parameters included for
[CSD3](https://docs.hpc.cam.ac.uk/hpc/) and [Ziggy](https://www.ch.cam.ac.uk/computing/ziggy-compute-server) (hunter group members).
A batch processor is also provided, which requires the [SSIP](https://gitlab.developers.cam.ac.uk/ch/hunter/ssip) code.

### How do I get Set up? ###

Guide to set up an environment with 

### Compilation of NWChem with python enabled ###

For this module to work it requires a suitably compiled version of NWChem (>v7.0.0) with python
enabled. Speak to your system administrator about how to go about creating a suitable module.

TODO: update instructions with requirements.

It is also possible to compile a local version (but this requires different settings (and use
of parameters specified in the JSON input file).

### Clone this repository ###

*From University of Cambridge gitlab (using ssh):*

        git clone git@gitlab.developers.cam.ac.uk:ch/hunter/ssiptools/mepsgen/nwchemcmlutils.git

If you have not already set up ssh access to the gitlab server please look through the instructions on the gitlab help page [https://gitlab.developers.cam.ac.uk/help/ssh/README](https://gitlab.developers.cam.ac.uk/help/ssh/README).


### Python Environment set up ###

Change in to the repository:

        cd nwchemcmlutils

Details of an anaconda environment is provided in the repository with the required dependencies for this package (with python 3.7).

        conda env create -f environment.yml

This creates and environment callled 'nwchempy3' which can be loaded using:

        conda activate nwchempy

#### Ziggy environment setup. ####

Note that if you are on ziggy you need to first load anaconda:

        module load anaconda/python3/2022.05

Then install this environment instead (based on python 3.6):

        conda env create -f environment36.yml

Activate environment with
        conda activate nwchempy36
        

### Install dependencies ###

There are a few more of custom modules that were created to handle the cml and xml files created in the process:
        
        git clone https://gitlab.developers.cam.ac.uk/ch/hunter/ssiptools/utils/xmlvalidator
        git clone https://gitlab.developers.cam.ac.uk:ch/hunter/ssiptools/mepsgen/cmlgenerator

And for the actual SSIP-calculating module

        git clone https://gitlab.developers.cam.ac.uk/ch/hunter/ssip

### Installing NWChemCMLUtils ###

Now that they're installed to use them in any directory, they need to be installed. Hence, for EACH newly installed repository, do

    pip install .

### Test CLI works ###

Test that the CLI works.

    python -m nwchemcmlutils -h
    usage: __main__.py [-h] {job_create,ssip_run} ...

nwchemcmlutils is a module containing the tools to calculate the MEPS surfaces
of molecules using NWChem, and also the batch processing of the SSIP coarse
graining for multiple molecules.

optional arguments:
  -h, --help            show this help message and exit

Sub methods:
  Two primary methods of use are available. Job file creation and
  footprinting.

  {job\_create,ssip\_run}
    job_create          Used to generate job submission files to run NWChem
                        calculation.
    ssip_run            Used to call ssip footprinting as a batch process
    
### SLURM system job creation ###    

The `job_create` option provides a CLI for creation of nwin and SLURM submission scripts for all CML files specified.


#### Custom JSON options ####

Options are:

job_name
account
partition
walltime
processors
nodes

modules
conda_version
conda_env
paths
utils_path
memory_limit

charge
multiplicity
exchange
iterations
maxiter
basis_functions


### Documentation ###

Documentation can be rendered using sphinx.

        cd docs
        make html

The rendered HTML can then be found in the build sub folder.

In progress: display of this documentation in a project wiki.

### Contribution guidelines ###

This code has been released under the AGPLv3 license.
If you find any bugs please file an issue ticket.
Submission of pull requests for open issues or improvements are welcomed.

### Who do I talk to? ###

Any queries please contact Kate Zator <kz265>.

### License

&copy; Mark Driver,  Christopher Hunter, Teodor Nikolov at the University of Cambridge

This is released under an AGPLv3 license for academic use.
Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 

Cambridge Enterprise Ltd
University of Cambridge
Hauser Forum
3 Charles Babbage Rd
Cambridge CB3 0GT
United Kingdom
Tel: +44 (0)1223 760339
Email: software@enterprise.cam.ac.uk
