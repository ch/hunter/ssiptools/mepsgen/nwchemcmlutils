#    nwchemcmlutils can generate MEPS files with NWChem and SSIP descriptions.
#    Copyright (C) 2019  Mark D. Driver
#
#    nwchemcmlutils is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
from setuptools import setup

setup(name='nwchemcmlutils',
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      description='CML processing within NWChem',
      url='https://gitlab.developers.cam.ac.uk/ch/hunter/ssiptools/mepsgen/nwchemcmlutils',
      author='Mark Driver',
      author_email='mdd31@cantab.ac.uk',
      license='AGPLv3',
      packages=setuptools.find_packages(),
      package_data={'':["resources/*.json","test/resources/*.*","test/resources/*/*","test/resources/*/*/*"]},
      zip_safe=False)
